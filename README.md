# MacType 1.13.1231 and a modified INI profile
A central location for the MacType program and a very good INI profile by renkun-ken, slightly modified.




## Purpose:

To replace the ugly Windows system font renderer with a FreeType renderer, ie. MacType. See: [FreeType](https://www.wikiwand.com/en/FreeType). 
I struggled to find the latest version of MacType so I thought why not share it along with the greatest profile made for MacType: [MacType.Decency]
(https://github.com/renkun-ken/MacType.Decency). 
MacType smoothes the font so it looks like the fonts rendered in Android or any other Linux OS. No more jagged ClearType font rendering.

### Differences between MacType with Decency.modified.English and Default:
*Default ClearType*

![Default ClearType](http://i.imgur.com/JGsoIwT.png)

*MacType + Decency*

![MacType + Decency](http://i.imgur.com/Zoksyp3.png)

## How to install:

1. Extract the archive `MacTypeInstaller+OriginalDecency.rar`
2. Install `MacTypeInstaller_2013_1231_0.exe`
3. Go to the `Original Decency by renkun-ken` folder.
4. Install `6216.ttf`
5. Copy `Decency.modified.English.ini` to the installation directory, eg. : `C:\Program Files (x86)\MacType\ini`
6. Run `MacWiz.exe` (the yellowish icon) as Admin.
7. Select `Registry` as the preferred mode. This way the fonts will be replaced system-wide.
8. Click `Next` and choose `Decency.modified.English`. Other options that look nice are: `XMac.LCD.HotShift@XHei(Android^Boldface^Ubuntu)` 
and `IOS` but Decency is still by far the best, there's no jagged edges on round letters like 'D' or 'O'


* You can also choose to not load the font renderer in certain programs by unloading the DLL in the `Decency.modified.English.INI`, viz. 

Go into `MacWiz.exe` and Click `Process manager` or `[P]` and unload the program from there.

Or in the INI:

```
[UnloadDll]
javaw.exe
JDownloader2.exe
```

* `MacTuner.exe` can also be run to fine tune font settings and access other advanced options.

######Feel free to contact me: mustaqim.malim@gmail.com for any queries, etc.
